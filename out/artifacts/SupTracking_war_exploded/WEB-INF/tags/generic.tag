<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@attribute name="footer" fragment="true" %>--%>
<%@attribute name="title" fragment="true" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/librairies/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/librairies/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <title><jsp:invoke fragment="title"/> - SupTracking</title>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">SupTracking</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/">Home</a></li>

                <c:choose>
                    <c:when test="${!empty sessionScope.user}">
                        <li><a href="${pageContext.request.contextPath}/auth/cars">Cars</a></li>
                        <%--<li><a href="${pageContext.request.contextPath}/auth/zones">Zones</a></li>--%>
                    </c:when>
                    <c:otherwise>
                        <li><a href="${pageContext.request.contextPath}/offer">Offer</a></li>
                    </c:otherwise>
                </c:choose>
                <c:if test="${sessionScope.loggedIn.equals('admin')}">
                    <li><a href="${pageContext.request.contextPath}/admin">Administration</a></li>
                </c:if>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <c:choose>
                    <c:when test="${!empty sessionScope.user}">
                        <li><a href="${pageContext.request.contextPath}/auth/profile">Account</a></li>
                        <li><a href="${pageContext.request.contextPath}/logout">Logout</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="${pageContext.request.contextPath}/login">Login</a></li>
                        <li><a href="${pageContext.request.contextPath}/register">Register</a></li>
                </c:otherwise>

                </c:choose>
            </ul>
        </div>
    </div>
</nav>

<div class="container" style="margin-top:100px; text-align:center;">
    <jsp:doBody/>
</div>

<%--<div id="pagefooter">--%>
    <%--<p id="copyright">Copyright 1927, Future Bits When There Be Bits Inc.</p>--%>
<%--</div>--%>

<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/librairies/validator.js"></script>
</body>
</html>