<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generic>
    <jsp:attribute name="title">Login</jsp:attribute>

    <jsp:body>
    <form class="form-signin" action="login" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>

        <div style="width: 30%; margin-left: 35%;">
            <label for="inputUsername" class="sr-only">Username</label>
            <input type="text" id="inputUsername" class="form-control" name="username" placeholder="Username" required autofocus>

            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required style="margin-top: 5px;">

            <a href="${pageContext.request.contextPath}/register" class="btn btn-lg btn-primary btn-block" style="margin-top: 5px;">Register</a>

            <button class="btn btn-lg btn-success btn-block" type="submit">Sign in</button>
        </div>
    </form>
    </jsp:body>

</t:generic>