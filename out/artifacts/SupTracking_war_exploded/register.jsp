<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generic>
  <jsp:attribute name="title">Register</jsp:attribute>

  <jsp:body>
    <form class="form-signin" action="register" method="post" data-toggle="validator" role="form">
      <h2 class="form-signin-heading">Register</h2>

      <div class="col-md-6 col-md-offset-3" style="margin-top: 20px;">
        <label for="inputUsername" class="sr-only">Username</label>
        <input type="text" id="inputUsername" name="username" class="form-control" placeholder="Username" required autofocus>

        <label for="inputFirstname" class="sr-only">Firstname</label>
        <input type="text" id="inputFirstname" name="firstname" class="form-control" placeholder="Firstname" required style="margin-top: 5px;">

        <label for="inputLastname" class="sr-only">Lastname</label>
        <input type="text" id="inputLastname" name="lastname" class="form-control" placeholder="Lastname" required style="margin-top: 5px;">

        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required style="margin-top: 5px;">

        <label for="inputPhone" class="sr-only">Phone number</label>
        <input type="text" id="inputPhone" name="phone" class="form-control" placeholder="Phone number" required style="margin-top: 5px;">

        <label for="inputCreditcard" class="sr-only">Creditcard number</label>
        <input type="number" id="inputCreditcard" name="creditCard" class="form-control" placeholder="Creditcard number" required style="margin-top: 5px;">

        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required style="margin-top: 5px;">

        <label for="inputPassword" class="sr-only">Retry Password</label>
        <input type="password" id="inputRetryPassword" class="form-control" data-match="#inputPassword" data-match-error="Whoops, these don't match" placeholder="Retry Password" required style="margin-top: 5px;">

        <div class="help-block with-errors"></div>
        <button class="btn btn-lg btn-success btn-block" type="submit" style="margin-top: 5px;">Register</button>
      </div>

    </form>
  </jsp:body>

</t:generic>