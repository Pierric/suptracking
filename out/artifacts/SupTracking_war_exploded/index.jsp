<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:generic>
  <jsp:attribute name="title">Home</jsp:attribute>

  <jsp:body>
    <h2 class="form-signin-heading">Welcome to SupTracking !</h2>
    <h4>Here is a list of all your cars.</h4>

    <div class="col-md-8 col-md-offset-2" style="margin-top: 20px;">
      <table class="table table-striped">
        <thead>
        <tr>
          <th>Car</th>
          <th>Latitude</th>
          <th>Longitude</th>
          <th>Last update</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${cars}" var="car">
          <tr>
            <td>${car.name}</td>
            <td>${car.latitude}</td>
            <td>${car.longitude}</td>
            <td>${car.lastUpdate}</td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>
  </jsp:body>

</t:generic>