<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:generic>
  <jsp:attribute name="title">Zones of ${car.brand} ${car.name}</jsp:attribute>

  <jsp:body>
    <div style="text-align: center">
      <h2 class="form-signin-heading">Zones of ${car.brand} ${car.name}</h2>
      <a class="btn btn-success" href="${pageContext.request.contextPath}/auth/addZone?car=${car.id}">
        <i class="fa fa-plus"></i> Add
      </a>
    </div>

    <div class="col-md-8 col-md-offset-2" style="margin-top: 20px;">
      <table class="table table-striped">
        <thead>
        <tr>
          <th>Zone</th>
          <th>Latitude</th>
          <th>Longitude</th>
          <th>Radius</th>
          <th>Start</th>
          <th>End</th>
          <th colspan="2"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${zones}" var="zone">
          <tr>
            <td>${zone.name}</td>
            <td>${zone.latitude}</td>
            <td>${zone.longitude}</td>
            <td>${zone.radius}</td>
            <td>${zone.startTime}</td>
            <td>${zone.endTime}</td>
            <td><a href="${pageContext.request.contextPath}/auth/updateZone?id=${zone.id}"><i class="fa fa-pencil fa-fw fa-lg" style="color: dodgerblue;"></i></a></td>
            <td><a href="${pageContext.request.contextPath}/auth/removeZone?id=${zone.id}"><i class="fa fa-trash-o fa-fw fa-lg" style="color: red;"></i></a></td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>
  </jsp:body>

</t:generic>