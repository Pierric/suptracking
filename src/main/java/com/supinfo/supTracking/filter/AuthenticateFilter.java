package com.supinfo.supTracking.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by pierr_000 on 26/03/2015.
 */
@WebFilter(filterName = "AuthenticateFilter", urlPatterns = "/auth/*")
public class AuthenticateFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getSession().getAttribute("loggedIn") != null) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        response.sendRedirect(request.getContextPath() + "/login");
    }

    @Override
    public void destroy() {

    }
}
