package com.supinfo.supTracking.rest;

import com.supinfo.supTracking.dao.DaoFactory;
import com.supinfo.supTracking.dao.jpa.JpaCarDao;
import com.supinfo.supTracking.dao.jpa.JpaUserDao;
import com.supinfo.supTracking.entity.Car;
import com.supinfo.supTracking.entity.Coords;
import com.supinfo.supTracking.entity.User;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Created by pierr_000 on 26/03/2015.
 */

@Path("/")
public class PositionResource {

    @POST
    public Response updatePosition(Coords coords) {

        if (coords.getType().equals("user")) {
            JpaUserDao jpaUserDao = DaoFactory.getJpaUserDao();
            User userToUpdate = jpaUserDao.getUserById(coords.getObjectId());

            if (userToUpdate == null) return Response.serverError().build();

            userToUpdate.setLatitude(coords.getLatitude());
            userToUpdate.setLongitude(coords.getLongitude());
            jpaUserDao.updateUser(userToUpdate);

            return Response.ok(userToUpdate).build();

        } else if (coords.getType().equals("car")) {
            JpaCarDao jpacarDao = DaoFactory.getJpaCarDao();
            Car carToUpdate = jpacarDao.getCarById(coords.getObjectId());

            if (carToUpdate == null) return Response.serverError().build();

            carToUpdate.setLatitude(coords.getLatitude());
            carToUpdate.setLongitude(coords.getLongitude());
            jpacarDao.updateCar(carToUpdate);

            return Response.ok(carToUpdate).build();
        }

        return Response.serverError().build();
    }
}
