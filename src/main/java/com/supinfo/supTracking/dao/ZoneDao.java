package com.supinfo.supTracking.dao;

import com.supinfo.supTracking.entity.Car;
import com.supinfo.supTracking.entity.Zone;

import java.util.List;

/**
 * Created by pierr_000 on 26/03/2015.
 */
public interface ZoneDao {

    Zone addZone(Zone zone);
    Zone getZoneById(int id);
    void updateZone(Zone zone);
    void removeZone(Zone zone);

    List<Zone> getAllZones(Car car);
    int getNumberOfZones();
}
