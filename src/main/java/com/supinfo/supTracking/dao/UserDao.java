package com.supinfo.supTracking.dao;

import com.supinfo.supTracking.entity.Coords;
import com.supinfo.supTracking.entity.User;

import java.util.List;

/**
 * Created by pierr_000 on 26/03/2015.
 */
public interface UserDao {

    User addUser(User user);
    User getUserById(int id);
    User getUserByUsername(String username);
    void updateUser(User user);
    void removeUser(User user);

    List<User> getAllUsers();
    int getNumberOfUsers();
}
