package com.supinfo.supTracking.dao;

import com.supinfo.supTracking.dao.jpa.JpaCarDao;
import com.supinfo.supTracking.dao.jpa.JpaUserDao;
import com.supinfo.supTracking.dao.jpa.JpaZoneDao;
import com.supinfo.supTracking.util.PersistenceManager;

/**
 * Created by pierr_000 on 26/03/2015.
 */
public class DaoFactory {

    private static JpaCarDao carService;
    private static JpaZoneDao zoneService;
    private static JpaUserDao userService;

    private DaoFactory() {}

    public static JpaZoneDao getJpaZoneDao() {
        if (zoneService == null)
            zoneService = new JpaZoneDao(PersistenceManager.getEntityManagerFactory());

        return zoneService;
    }

    public static JpaCarDao getJpaCarDao() {
        if (carService == null)
            carService = new JpaCarDao(PersistenceManager.getEntityManagerFactory());

        return carService;
    }

    public static JpaUserDao getJpaUserDao() {
        if (userService == null)
            userService = new JpaUserDao(PersistenceManager.getEntityManagerFactory());

        return userService;
    }
}
