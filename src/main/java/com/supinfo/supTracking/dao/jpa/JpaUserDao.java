package com.supinfo.supTracking.dao.jpa;

import com.supinfo.supTracking.dao.UserDao;
import com.supinfo.supTracking.entity.Coords;
import com.supinfo.supTracking.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by pierr_000 on 26/03/2015.
 */
public class JpaUserDao implements UserDao {

    private EntityManagerFactory emf;

    public JpaUserDao(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public User addUser(User user) {
        User result = null;
        EntityManager em = emf.createEntityManager();
        EntityTransaction t = em.getTransaction();

        try {
            t.begin();
            em.persist(user);
            t.commit();
            result = user;
        } finally {
            if (t.isActive()) t.rollback();
            em.close();
        }

        return result;
    }

    @Override
    public User getUserById(int id) {
        EntityManager em = emf.createEntityManager();
        User result = em.find(User.class, id);
        em.close();

        return result;
    }

    @Override
    public User getUserByUsername(String username) {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT u FROM User AS u WHERE u.username = :username").setParameter("username", username);
        User result = (User) query.getSingleResult();
        em.close();

        return result;
    }

    @Override
    public void updateUser(User user) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction t = em.getTransaction();

        try {
            t.begin();
            em.merge(user);
            t.commit();
        } finally {
            if (t.isActive()) t.rollback();
            em.close();
        }
    }

    @Override
    public void removeUser(User user) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction t = em.getTransaction();

        try {
            t.begin();
            em.remove(em.merge(user));
            t.commit();
        } finally {
            if (t.isActive()) t.rollback();
            em.close();
        }
    }

    @Override
    public List<User> getAllUsers() {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT u FROM User as u");
        List<User> users = (List<User>) query.getResultList();
        em.close();

        return users;
    }

    @Override
    public int getNumberOfUsers() {
        EntityManager em = emf.createEntityManager();
        String queryString = "SELECT Count(*) FROM users";
        Query query = em.createNativeQuery(queryString);
        return ((BigInteger) query.getSingleResult()).intValue();
    }
}
