package com.supinfo.supTracking.dao.jpa;

import com.supinfo.supTracking.dao.CarDao;
import com.supinfo.supTracking.entity.Car;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by pierr_000 on 26/03/2015.
 */
public class JpaCarDao implements CarDao {

    private EntityManagerFactory emf;

    public JpaCarDao(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public Car addCar(Car car) {
        Car result = null;
        EntityManager em = emf.createEntityManager();
        EntityTransaction t = em.getTransaction();

        try {
            t.begin();
            em.persist(car);
            t.commit();
            result = car;
        } finally {
            if (t.isActive()) t.rollback();
            em.close();
        }

        return result;
    }

    @Override
    public Car getCarById(int id) {
        EntityManager em = emf.createEntityManager();
        Car result = em.find(Car.class, id);
        em.close();

        return result;
    }

    @Override
    public void updateCar(Car car) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction t = em.getTransaction();

        try {
            t.begin();
            em.merge(car);
            t.commit();
        } finally {
            if (t.isActive()) t.rollback();
            em.close();
        }
    }

    @Override
    public void removeCar(Car car) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction t = em.getTransaction();

        try {
            t.begin();
            em.remove(em.merge(car));
            t.commit();
        } finally {
            if (t.isActive()) t.rollback();
            em.close();
        }
    }

    @Override
    public List<Car> getAllCars(int userid) {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT c FROM Car as c WHERE c.user.id = :id").setParameter("id", userid);
        List<Car> cars = (List<Car>) query.getResultList();
        em.close();

        return cars;
    }

    @Override
    public int getNumberOfCars() {
        EntityManager em = emf.createEntityManager();
        String queryString = "SELECT Count(*) FROM cars";
        Query query = em.createNativeQuery(queryString);
        return ((BigInteger) query.getSingleResult()).intValue();
    }
}
