package com.supinfo.supTracking.dao.jpa;

import com.supinfo.supTracking.dao.ZoneDao;
import com.supinfo.supTracking.entity.Car;
import com.supinfo.supTracking.entity.Zone;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by pierr_000 on 26/03/2015.
 */
public class JpaZoneDao implements ZoneDao {

    private EntityManagerFactory emf;

    public JpaZoneDao(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public Zone addZone(Zone zone) {
        Zone result = null;
        EntityManager em = emf.createEntityManager();
        EntityTransaction t = em.getTransaction();

        try {
            t.begin();
            em.persist(zone);
            t.commit();
            result = zone;
        } finally {
            if (t.isActive()) t.rollback();
            em.close();
        }

        return result;
    }

    @Override
    public Zone getZoneById(int id) {
        EntityManager em = emf.createEntityManager();
        Zone result = em.find(Zone.class, id);
        em.close();

        return result;
    }

    @Override
    public void updateZone(Zone zone) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction t = em.getTransaction();

        try {
            t.begin();
            em.merge(zone);
            t.commit();
        } finally {
            if (t.isActive()) t.rollback();
            em.close();
        }
    }

    @Override
    public void removeZone(Zone zone) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction t = em.getTransaction();

        try {
            t.begin();
            em.remove(em.merge(zone));
            t.commit();
        } finally {
            if (t.isActive()) t.rollback();
            em.close();
        }
    }

    @Override
    public List<Zone> getAllZones(Car car) {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT z FROM Zone as z WHERE z.car.id = :id").setParameter("id", car.getId());
        List<Zone> zones = (List<Zone>) query.getResultList();
        em.close();

        return zones;
    }

    @Override
    public int getNumberOfZones() {
        EntityManager em = emf.createEntityManager();
        String queryString = "SELECT Count(*) FROM zones";
        Query query = em.createNativeQuery(queryString);
        return ((BigInteger) query.getSingleResult()).intValue();
    }
}
