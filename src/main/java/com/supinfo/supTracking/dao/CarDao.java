package com.supinfo.supTracking.dao;

import com.supinfo.supTracking.entity.Car;

import java.util.List;

/**
 * Created by pierr_000 on 26/03/2015.
 */
public interface CarDao {

    Car addCar(Car car);
    Car getCarById(int id);
    void updateCar(Car car);
    void removeCar(Car car);

    List<Car> getAllCars(int userid);
    int getNumberOfCars();
}
