package com.supinfo.supTracking.servlet;

import com.supinfo.supTracking.dao.DaoFactory;
import com.supinfo.supTracking.entity.Car;
import com.supinfo.supTracking.entity.Zone;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by charles on 27/03/2015.
 */

@WebServlet(name = "AddZoneServlet", urlPatterns = "/auth/addZone")
public class AddZoneServlet extends HttpServlet {
//    private static final long serialVersionUID = 159753L;

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Car car;
        try {
            car = DaoFactory.getJpaCarDao().getCarById(Integer.parseInt(req.getParameter("car")));
        } catch (Exception e) {
            res.sendError(500, "Error while getting this car");
            return;
        }

        if (car.getUser().getId() != Integer.parseInt(req.getSession().getAttribute("user").toString())) {
            res.sendError(401, "Not allowed to modify this car !");
            return;
        }
        
        float latitude;
        float longitude;
        float radius;
        long startTime;
        long endTime;

        try {
            latitude = Float.parseFloat(req.getParameter("latitude"));
        } catch (NumberFormatException e) {
            latitude = 0;
        }

        try {
            longitude = Float.parseFloat(req.getParameter("longitude"));
        } catch (NumberFormatException e) {
            longitude = 0;
        }

        try {
            radius = Float.parseFloat(req.getParameter("radius"));
        } catch (NumberFormatException e) {
            radius = 0;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm");

        try {
            startTime = dateFormat.parse(req.getParameter("startTime")).getTime();
        } catch (NullPointerException e) {
            startTime = 0;
        } catch (ParseException e) {
            startTime = 0;
        }

        try {
            endTime = dateFormat.parse(req.getParameter("endTime")).getTime();
        } catch (NullPointerException e) {
            endTime = 0;
        } catch (ParseException e) {
            endTime = 0;
        }

        String name = req.getParameter("name").trim();

        Zone zone = new Zone();
        zone.setName(name);
        zone.setLatitude(latitude);
        zone.setLongitude(longitude);
        zone.setRadius(radius);
        zone.setStartTime(startTime);
        zone.setEndTime(endTime);
        zone.setCar(car);

        DaoFactory.getJpaZoneDao().addZone(zone);

        res.sendRedirect(req.getContextPath() + "/auth/zones?car=" + car.getId());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Car car = DaoFactory.getJpaCarDao().getCarById(Integer.parseInt(req.getParameter("car")));

            if (car.getUser().getId() != Integer.parseInt(req.getSession().getAttribute("user").toString())) {
                resp.sendError(401, "Not allowed to add a zone to this car !");
                return;
            }

            req.setAttribute("car", car.getId());
            RequestDispatcher rd = req.getRequestDispatcher("/auth/addZone.jsp");
            rd.forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            resp.sendRedirect(req.getContextPath() + "/cars");
        }

    }
}
