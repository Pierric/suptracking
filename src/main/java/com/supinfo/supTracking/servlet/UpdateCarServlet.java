package com.supinfo.supTracking.servlet;

import com.supinfo.supTracking.dao.DaoFactory;
import com.supinfo.supTracking.entity.Car;
import com.supinfo.supTracking.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Created by charles on 27/03/2015.
 */

@WebServlet(name = "UpdateCarServlet", urlPatterns = "/auth/updateCar")
public class UpdateCarServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Car car;
        try {
            car = DaoFactory.getJpaCarDao().getCarById(Integer.parseInt(req.getParameter("id")));
        } catch (Exception e) {
            res.sendError(500, "Error while getting this car");
            return;
        }

        if (car.getUser().getId() != Integer.parseInt(req.getSession().getAttribute("user").toString())) {
            res.sendError(401, "Not allowed to modify this car !");
            return;
        }

        String name = req.getParameter("name").trim();
        String brand = req.getParameter ("brand").trim();

        try {
            car.setYear(Integer.parseInt(req.getParameter("year")));
        } catch (NumberFormatException e) {}

        try {
            car.setLatitude(Float.parseFloat(req.getParameter("latitude")));
        } catch (NumberFormatException e) {}

        try {
            car.setLongitude(Float.parseFloat(req.getParameter("longitude")));
        } catch (NumberFormatException e) {}

        long lastUpdate = new Date().getTime();

        car.setName(name);
        car.setBrand(brand);
        car.setLastUpdate(lastUpdate);

        DaoFactory.getJpaCarDao().updateCar(car);
        res.sendRedirect(req.getContextPath() + "/auth/cars");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Car car = DaoFactory.getJpaCarDao().getCarById(Integer.parseInt(req.getParameter("id")));

            if (car.getUser().getId() != Integer.parseInt(req.getSession().getAttribute("user").toString())) {
                resp.sendError(401, "Not allowed to modify this car !");
                return;
            }

            req.setAttribute("car", car);

            RequestDispatcher rd = req.getRequestDispatcher("/auth/updateCar.jsp");
            rd.forward(req, resp);
        } catch (Exception e) {
            resp.sendRedirect(req.getContextPath() + "/auth/cars");
        }
    }
}
