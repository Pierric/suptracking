package com.supinfo.supTracking.servlet;

import com.supinfo.supTracking.dao.DaoFactory;
import com.supinfo.supTracking.entity.Car;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by charles on 27/03/2015.
 */

@WebServlet(name = "RemoveCarServlet", urlPatterns = "/auth/removeCar")
public class RemoveCarServlet extends HttpServlet {
//    private static final long serialVersionUID = 159753L;

//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
//        try {
//            Car car = DaoFactory.getJpaCarDao().getCarById(Integer.parseInt(req.getParameter("id")));
//            DaoFactory.getJpaCarDao().removeCar(car);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            res.sendRedirect(req.getContextPath() + "/cars");
//        }
//    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Car car = DaoFactory.getJpaCarDao().getCarById(Integer.parseInt(req.getParameter("id")));

            if (car.getUser().getId() != Integer.parseInt(req.getSession().getAttribute("user").toString())) {
                resp.sendError(401, "Not allowed to remove this car !");
                return;
            }

            DaoFactory.getJpaCarDao().removeCar(car);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            resp.sendRedirect(req.getContextPath() + "/auth/cars");
        }
    }
}
