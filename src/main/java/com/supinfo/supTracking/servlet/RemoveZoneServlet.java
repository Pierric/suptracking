package com.supinfo.supTracking.servlet;

import com.supinfo.supTracking.dao.DaoFactory;
import com.supinfo.supTracking.dao.ZoneDao;
import com.supinfo.supTracking.entity.Car;
import com.supinfo.supTracking.entity.Zone;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOError;
import java.io.IOException;

/**
 * Created by charles on 27/03/2015.
 */

@WebServlet(name = "RemoveZoneServlet", urlPatterns = "/auth/removeZone")
public class RemoveZoneServlet extends HttpServlet {
//    private static final long seriaVersionIUD = 159753L;

//    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
//        try {
//            Zone zone = DaoFactory.getJpaZoneDao().getZoneById(Integer.parseInt(req.getParameter("id")));
//            DaoFactory.getJpaZoneDao().removeZone(zone);
//        }catch (NumberFormatException e){
//            e.printStackTrace();
//        }finally {
//            res.sendRedirect(req.getContextPath()+"/...");
//        }
//
//    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Zone zone = DaoFactory.getJpaZoneDao().getZoneById(Integer.parseInt(req.getParameter("id")));
            int userId = zone.getCar().getUser().getId();

            if (userId != Integer.parseInt(req.getSession().getAttribute("user").toString())) {
                resp.sendError(401, "Not allowed to modify this car !");
                return;
            }

            DaoFactory.getJpaZoneDao().removeZone(zone);
            resp.sendRedirect(req.getContextPath() + "/auth/zones?car=" + zone.getCar().getId());
        } catch (NumberFormatException e){
            e.printStackTrace();
            resp.sendRedirect(req.getContextPath() + "/auth/cars");
        }
    }
}
