package com.supinfo.supTracking.servlet;

import com.supinfo.supTracking.dao.DaoFactory;
import com.supinfo.supTracking.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Created by charles on 27/03/2015.
 */

@WebServlet(name = "UpdateUserServlet", urlPatterns = "/auth/update")
public class UpdateUserServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        User user = DaoFactory.getJpaUserDao().getUserById(Integer.parseInt(req.getSession().getAttribute("user").toString()));

        try {
            user.setZip(Integer.parseInt(req.getParameter("ZIP")));
        } catch (NumberFormatException e) {}

        try {
            user.setCreditCard(Integer.parseInt(req.getParameter("creditCard")));
        } catch (NumberFormatException e) {}

        try {
            user.setPhone(Integer.parseInt(req.getParameter("phone")));
        } catch (NumberFormatException e) {}

        try {
            user.setLatitude(Float.parseFloat(req.getParameter("latitude")));
        } catch (NumberFormatException e) {}

        try {
            user.setLongitude(Float.parseFloat(req.getParameter("longitude")));
        } catch (NumberFormatException e) {}

        String email = req.getParameter("email").trim();
        String lastName = req.getParameter("lastname").trim();
        String firstname = req.getParameter("firstname").trim();
        String street = req.getParameter("street").trim();
        String city = req.getParameter("city").trim();

        user.setEmail(email);
        user.setLastName(lastName);
        user.setFirstName(firstname);
        user.setStreet(street);
        user.setCity(city);
        user.setLastUpdate(new Date().getTime());

        DaoFactory.getJpaUserDao().updateUser(user);
        res.sendRedirect(req.getContextPath() + "/auth/profile");
    }
}
