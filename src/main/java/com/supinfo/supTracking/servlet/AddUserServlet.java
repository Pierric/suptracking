package com.supinfo.supTracking.servlet;

import com.supinfo.supTracking.dao.DaoFactory;
import com.supinfo.supTracking.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 * Created by charles on 27/03/2015.
 */

@WebServlet(name = "AddUserServlet", urlPatterns = "/register")
public class AddUserServlet extends HttpServlet {
//    private static final long serialVersionUID = 159753L;

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String username = req.getParameter("username").trim();
        String firstname = req.getParameter("firstname").trim();
        String lastName = req.getParameter("lastname").trim();
        String email = req.getParameter("email").trim();
        String password = req.getParameter("password").trim();
//        String street = req.getParameter("street").trim();
//        String city = req.getParameter("city").trim();

//        int zip;
        int creditCard;
        int phone;
//        long lastUpdate;
//        float latitude;
//        float longitude;

//        try {
//            zip = Integer.parseInt(req.getParameter("ZIP"));
//        } catch (NumberFormatException e) {
//            zip = 0;
//        }

        try {
            creditCard = Integer.parseInt(req.getParameter("creditCard"));
        } catch (NumberFormatException e) {
            creditCard = 0;
        }

        try {
            phone = Integer.parseInt(req.getParameter("phone"));
        } catch (NumberFormatException e){
            phone = 0;
        }

//        try {
//            lastUpdate = Long.parseLong(req.getParameter("lastUpdate"));
//        } catch (NumberFormatException e){
//            lastUpdate = 0;
//        }

//        try {
//            latitude = Float.parseFloat(req.getParameter("latitude"));
//        } catch (NumberFormatException e){
//            latitude = 0;
//        }

//        try {
//            longitude = Float.parseFloat(req.getParameter("longitude"));
//        } catch (NumberFormatException e){
//            longitude = 0;
//        }

        User user = new User();
        user.setUsername(username);
        user.setEmail(email);
        user.setLastName(lastName);
        user.setFirstName(firstname);
//        user.setStreet(street);
//        user.setCity(city);
//        user.setZip(zip);
        user.setCreditCard(creditCard);
        user.setPhone(phone);
        user.setLastUpdate(new Date().getTime());
//        user.setLatitude(latitude);
//        user.setLongitude(longitude);

        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(password.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        user.setPassword(generatedPassword);
        User createdUser = DaoFactory.getJpaUserDao().addUser(user);

        HttpSession session = req.getSession();
        session.setAttribute("loggedIn", "user");
        session.setAttribute("user", createdUser.getId());

        res.sendRedirect(req.getContextPath() + "/");
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (req.getSession().getAttribute("loggedIn") != null) {
            res.sendRedirect(req.getContextPath() + "/");
            return;
        }

        RequestDispatcher rd = req.getRequestDispatcher("/register.jsp");
        rd.forward(req, res);
    }

}
