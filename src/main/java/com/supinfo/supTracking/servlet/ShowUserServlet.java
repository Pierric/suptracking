package com.supinfo.supTracking.servlet;

import com.supinfo.supTracking.dao.DaoFactory;
import com.supinfo.supTracking.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.UnknownHostException;

/**
 * Created by charles on 27/03/2015.
 */

@WebServlet(name = "ShowUserServlet", urlPatterns = "/auth/profile")
public class ShowUserServlet extends HttpServlet {
//    private static final long serialVersionUID = 159753L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try {
            int id = Integer.parseInt(req.getSession().getAttribute("user").toString());

            User user = DaoFactory.getJpaUserDao().getUserById(id);
            req.setAttribute("user", user);
            req.getRequestDispatcher("/auth/profile.jsp").forward(req, res);

        } catch (Exception e) {
            e.printStackTrace();
            res.sendRedirect(req.getContextPath() + "/");
        }
    }
}
