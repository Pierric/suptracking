package com.supinfo.supTracking.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by pierr_000 on 31/03/2015.
 */

@WebServlet(name = "OfferServlet", urlPatterns = "/offer")
public class OfferServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/offer.jsp").forward(req, resp);
    }
}
