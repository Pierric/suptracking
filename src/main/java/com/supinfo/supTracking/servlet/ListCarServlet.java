package com.supinfo.supTracking.servlet;

import com.supinfo.supTracking.dao.DaoFactory;
import com.supinfo.supTracking.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by charles on 27/03/2015.
 */

@WebServlet(name = "ListCarServlet", urlPatterns = "/auth/cars")
public class ListCarServlet extends HttpServlet {
//    private static final long serialVersionUID = 159753L;

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        int userid = Integer.parseInt(req.getSession().getAttribute("user").toString());
        req.setAttribute("cars", DaoFactory.getJpaCarDao().getAllCars(userid));

        RequestDispatcher rd = req.getRequestDispatcher("/auth/cars.jsp");
        rd.forward(req,res);
    }

}
