package com.supinfo.supTracking.servlet;

import com.supinfo.supTracking.dao.DaoFactory;
import com.supinfo.supTracking.dao.jpa.JpaUserDao;
import com.supinfo.supTracking.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by pierr_000 on 31/03/2015.
 */

@WebServlet(name = "RemoveUserServlet", urlPatterns = "/admin/removeUser")
public class RemoveUserServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int id = Integer.parseInt(req.getParameter("id"));
            JpaUserDao jpaUserDao = DaoFactory.getJpaUserDao();

            User user = jpaUserDao.getUserById(id);
            jpaUserDao.removeUser(user);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            resp.sendRedirect(req.getContextPath() + "/admin");
        }
    }
}
