package com.supinfo.supTracking.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.supTracking.dao.DaoFactory;
import com.supinfo.supTracking.entity.Car;
import com.supinfo.supTracking.entity.User;

import java.io.IOException;
import java.util.Date;


@WebServlet(name = "AddCarServlet", urlPatterns = "/auth/addCar")
public class AddCarServlet extends HttpServlet {
//    private static final long serialVersionUID = 159753L;

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String name = req.getParameter("name").trim();
        String brand = req.getParameter ("brand").trim();

        int year;
        float latitude;
        float longitude;

        try {
            year = Integer.parseInt(req.getParameter("year"));
        } catch (NumberFormatException e) {
            year = 0;
        }

        try {
            latitude = Float.parseFloat(req.getParameter("latitude"));
        } catch (NumberFormatException e) {
            latitude = 0;
        }

        try {
            longitude = Float.parseFloat(req.getParameter("longitude"));
        } catch (NumberFormatException e){
            longitude = 0;
        }

        long lastUpdate = new Date().getTime();
        User user = DaoFactory.getJpaUserDao().getUserById(Integer.parseInt(req.getSession().getAttribute("user").toString()));

        Car car = new Car();
        car.setName(name);
        car.setBrand(brand);
        car.setYear(year);
        car.setLatitude(latitude);
        car.setLatitude(longitude);
        car.setLastUpdate(lastUpdate);
        car.setUser(user);

        DaoFactory.getJpaCarDao().addCar(car);

        res.sendRedirect(req.getContextPath() + "/auth/cars");
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        RequestDispatcher rd = req.getRequestDispatcher("/auth/addCar.jsp");
        rd.forward(req, res);
    }

}
