package com.supinfo.supTracking.servlet;

import com.supinfo.supTracking.dao.DaoFactory;
import com.supinfo.supTracking.entity.Car;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by pierr_000 on 30/03/2015.
 */

@WebServlet(name = "ListZoneServlet", urlPatterns = "/auth/zones")
public class ListZoneServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Car car;
        try {
            car = DaoFactory.getJpaCarDao().getCarById(Integer.parseInt(req.getParameter("car")));
        } catch (Exception e) {
            res.sendError(500, "Error while getting this cars' zones");
            return;
        }

        if (car.getUser().getId() != Integer.parseInt(req.getSession().getAttribute("user").toString())) {
            res.sendError(401, "Not allowed to access this car !");
            return;
        }

        req.setAttribute("zones", DaoFactory.getJpaZoneDao().getAllZones(car));
        req.setAttribute("car", car);
        RequestDispatcher rd = req.getRequestDispatcher("/auth/zones.jsp");
        rd.forward(req,res);
    }

}
