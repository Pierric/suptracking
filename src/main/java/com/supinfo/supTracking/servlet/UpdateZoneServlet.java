package com.supinfo.supTracking.servlet;

import com.supinfo.supTracking.dao.DaoFactory;
import com.supinfo.supTracking.entity.Car;
import com.supinfo.supTracking.entity.Zone;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by charles on 27/03/2015.
 */

@WebServlet(name = "UpdateZoneServlet", urlPatterns = "/auth/updateZone")
public class UpdateZoneServlet extends HttpServlet {
//    private static final long serialVersionUID = 159753L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Zone zone;
        try {
            zone = DaoFactory.getJpaZoneDao().getZoneById(Integer.parseInt(req.getParameter("id")));
        } catch (Exception e) {
            res.sendError(500, "Error while getting this zone");
            return;
        }

        if (zone.getCar().getUser().getId() != Integer.parseInt(req.getSession().getAttribute("user").toString())) {
            res.sendError(401, "Not allowed to modify this zone !");
            return;
        }

        try {
            zone.setLatitude(Float.parseFloat(req.getParameter("latitude")));
        } catch (NumberFormatException e) {}

        try {
            zone.setLongitude(Float.parseFloat(req.getParameter("longitude")));
        } catch (NumberFormatException e) {}

        try {
            zone.setRadius(Float.parseFloat(req.getParameter("radius")));
        } catch (NumberFormatException e) {}

        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm");

        try {
            zone.setStartTime(dateFormat.parse(req.getParameter("startTime")).getTime());
        } catch (Exception e) {}

        try {
            zone.setEndTime(dateFormat.parse(req.getParameter("endTime")).getTime());
        } catch (Exception e) {}

        zone.setName(req.getParameter("name").trim());
        DaoFactory.getJpaZoneDao().updateZone(zone);

        res.sendRedirect(req.getContextPath() + "/auth/zones?car=" + zone.getCar().getId());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Zone zone = DaoFactory.getJpaZoneDao().getZoneById(Integer.parseInt(req.getParameter("id")));

            if (zone.getCar().getUser().getId() != Integer.parseInt(req.getSession().getAttribute("user").toString())) {
                resp.sendError(401, "Not allowed to modify this zone !");
                return;
            }

            req.setAttribute("zone", zone);
            RequestDispatcher rd = req.getRequestDispatcher("/auth/updateZone.jsp");
            rd.forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            resp.sendRedirect(req.getContextPath() + "/auth/cars");
        }
    }
}
