package com.supinfo.supTracking.servlet;

import com.supinfo.supTracking.dao.DaoFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by charles on 27/03/2015.
 */

@WebServlet(name = "IndexServlet", urlPatterns = "/")
public class IndexServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("loggedIn") != null) {
            int userid = Integer.parseInt(req.getSession().getAttribute("user").toString());
            req.setAttribute("cars", DaoFactory.getJpaCarDao().getAllCars(userid));

            RequestDispatcher rd = req.getRequestDispatcher("/index.jsp");
            rd.forward(req, resp);
            return;
        }

        int nbUsers = DaoFactory.getJpaUserDao().getNumberOfUsers();
        int nbCars = DaoFactory.getJpaCarDao().getNumberOfCars();
        int nbZones = DaoFactory.getJpaZoneDao().getNumberOfZones();

        req.setAttribute("nbcars", nbCars);
        req.setAttribute("nbUsers", nbUsers);
        req.setAttribute("nbZones", nbZones);

        RequestDispatcher rd = req.getRequestDispatcher("/anonymousIndex.jsp");
        rd.forward(req, resp);
    }
}
