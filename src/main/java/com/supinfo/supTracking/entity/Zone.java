package com.supinfo.supTracking.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by pierr_000 on 25/03/2015.
 */

@Entity
@Table(name = "zones")
public class Zone implements Serializable {

    @Id
    @GeneratedValue()
    private int id;

    private String name;
    private float latitude;
    private float longitude;
    private float radius;
    private Long startTime;
    private Long endTime;

    @ManyToOne
    @JoinColumn(name = "car_fk")
    private Car car;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}
