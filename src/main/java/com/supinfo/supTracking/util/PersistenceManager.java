package com.supinfo.supTracking.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by pierr_000 on 26/03/2015.
 */
public class PersistenceManager {
    private static EntityManagerFactory emf;

    private PersistenceManager() {}

    public static EntityManagerFactory getEntityManagerFactory() {
        if (emf == null) emf = Persistence.createEntityManagerFactory("persistenceUnit");
        return emf;
    }

    public static void closeEntityManagerFactory() {
        if (emf != null && emf.isOpen()) emf.close();
    }
}
