package com.supinfo.supTracking.listener;

import com.supinfo.supTracking.util.PersistenceManager;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by pierr_000 on 26/03/2015.
 */
public class PersistenceAppListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        PersistenceManager.closeEntityManagerFactory();
    }
}
