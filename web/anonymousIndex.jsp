<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generic>
  <jsp:attribute name="title">Home</jsp:attribute>

  <jsp:body>

    <h2 class="form-signin-heading">Presentation</h2>
    <p>SupTracking vous permet de définir des zones de stationnement pour vos véhicules et vous alerte lorsque l'un d'entre eux sort de celles-ci !</p>
    <h4>Statistiques : ${nbcars} voitures, ${nbZones} zones et ${nbUsers} utilisateurs</h4>

  </jsp:body>

</t:generic>