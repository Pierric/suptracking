<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:generic>
  <jsp:attribute name="title">New car</jsp:attribute>

  <jsp:body>
    <form class="form-signin" action="${pageContext.request.contextPath}/auth/addCar" method="post">

      <h2 class="form-signin-heading">Create Car</h2>

      <div class="col-md-6 col-md-offset-3" style="margin-top: 20px;">
        <label for="inputCarName" class="sr-only">Name</label>
        <input type="text" id="inputCarName" class="form-control" name="name" placeholder="Name" required autofocus>

        <label for="inputBrand" class="sr-only">Brand</label>
        <input type="text" id="inputBrand" class="form-control" name="brand" placeholder="Brand" style="margin-top: 5px;" required>

        <label for="inputYear" class="sr-only">Year</label>
        <input type="number" id="inputYear" class="form-control" name="year" placeholder="Year of first entry" style="margin-top: 5px;" required>

        <label for="inputLat" class="sr-only">Latitude</label>
        <input type="number" id="inputLat" class="form-control" name="latitude" step="0.01" placeholder="Latitude" style="margin-top: 5px;" required>

        <label for="inputLong" class="sr-only">Longitude</label>
        <input type="number" id="inputLong" class="form-control" name="longitude" step="0.01" placeholder="Longitude" style="margin-top: 5px;" required>

        <button class="btn btn-lg btn-success btn-block" type="submit" style="margin-top: 5px;">Create</button>
      </div>

    </form>
  </jsp:body>

</t:generic>