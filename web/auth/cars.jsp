<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:generic>
  <jsp:attribute name="title">Cars</jsp:attribute>

  <jsp:body>
    <div style="text-align: center">
      <h2 class="form-signin-heading">List of cars</h2>
      <a class="btn btn-success" href="${pageContext.request.contextPath}/auth/addCar">
        <i class="fa fa-plus"></i> Add
      </a>
    </div>

    <div class="col-md-8 col-md-offset-2" style="margin-top: 20px;">
      <table class="table table-striped">
        <thead>
        <tr>
          <th>Car</th>
          <th>Brand</th>
          <th>Year of entry</th>
          <th>Latitude</th>
          <th>Longitude</th>
          <th>Last update</th>
          <th colspan="3"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${cars}" var="car">
          <tr>
            <td>${car.name}</td>
            <td>${car.brand}</td>
            <td>${car.year}</td>
            <td>${car.latitude}</td>
            <td>${car.longitude}</td>
            <td>${car.lastUpdate}</td>
            <td><a href="${pageContext.request.contextPath}/auth/zones?car=${car.id}" class="btn btn-sm btn-warning" style="padding: 3px 10px;">Zones</a></td>
            <td><a href="${pageContext.request.contextPath}/auth/updateCar?id=${car.id}"><i class="fa fa-pencil fa-fw fa-lg" style="color: dodgerblue;"></i></a></td>
            <td><a href="${pageContext.request.contextPath}/auth/removeCar?id=${car.id}"><i class="fa fa-trash-o fa-fw fa-lg" style="color: red;"></i></a></td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>
  </jsp:body>

</t:generic>