<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:generic>
  <jsp:attribute name="title">Account</jsp:attribute>

  <jsp:body>
    <form class="form-signin" action="${pageContext.request.contextPath}/auth/update" method="post">
      <h2 class="form-signin-heading">Account</h2>

      <input hidden name="id" value="${user.id}">

      <div class="col-md-6 col-md-offset-3" style="margin-top: 20px; text-align: left;">
        <label for="inputUsername">Username</label>
        <input type="text" id="inputUsername" class="form-control" name="username" value="${user.username}" placeholder="Username" readonly>

        <label for="inputFirstname">Firstname</label>
        <input type="text" id="inputFirstname" class="form-control" name="firstname" value="${user.firstName}" placeholder="Firstname" required autofocus style="margin-top: 5px;">

        <label for="inputLastname">Lastname</label>
        <input type="text" id="inputLastname" class="form-control" name="lastname" value="${user.lastName}" placeholder="Lastname" required style="margin-top: 5px;">

        <label for="inputEmail">Email address</label>
        <input type="email" id="inputEmail" class="form-control" name="email" value="${user.email}" placeholder="Email address" required style="margin-top: 5px;">

        <label for="inputPhone">Phone number</label>
        <input type="text" id="inputPhone" class="form-control" name="phone" value="${user.phone}" placeholder="Phone number" required style="margin-top: 5px;">

        <label for="inputCreditcard">Creditcard number</label>
        <input type="number" id="inputCreditcard" class="form-control" name="creditCard" value="${user.creditCard}" placeholder="Creditcard number" required style="margin-top: 5px;">

        <label for="inputStreet">Street</label>
        <input type="text" id="inputStreet" class="form-control" name="street" value="${user.street}" placeholder="Street" required style="margin-top: 5px;">

        <label for="inputCode">Zip Code</label>
        <input type="Number" id="inputCode" class="form-control" name="ZIP" value="${user.zip}" placeholder="Zip code" required style="margin-top: 5px;">

        <label for="inputCity">City</label>
        <input type="text" id="inputCity" class="form-control" name="city" value="${user.city}" placeholder="City" required style="margin-top: 5px;">

        <button class="btn btn-lg btn-success btn-block" type="submit" style="margin-top: 5px;">Send</button>
      </div>

    </form>
  </jsp:body>

</t:generic>