<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:generic>
  <jsp:attribute name="title">New zone</jsp:attribute>

  <jsp:body>
    <form class="form-signin" action="${pageContext.request.contextPath}/auth/addZone" method="post">

      <h2 class="form-signin-heading">Create Zone</h2>
      <input type="hidden" name="car" value="${car}">

      <div class="col-md-6 col-md-offset-3" style="margin-top: 20px;">
        <label for="inputZoneName" class="sr-only">Name</label>
        <input type="text" id="inputZoneName" class="form-control" name="name" placeholder="Name" required autofocus>

        <label for="inputLat" class="sr-only">Latitude</label>
        <input type="number" id="inputLat" class="form-control" name="latitude" step="0.01" placeholder="Latitude" style="margin-top: 5px;" required>

        <label for="inputLong" class="sr-only">Longitude</label>
        <input type="number" id="inputLong" class="form-control" name="longitude" step="0.01" placeholder="Longitude" style="margin-top: 5px;" required>

        <label for="inputRad" class="sr-only">Radius</label>
        <input type="number" id="inputRad" class="form-control" name="radius" placeholder="Radius" style="margin-top: 5px;" required>

        <label for="inputStart" class="sr-only">Start Time</label>
        <input type="time" id="inputStart" class="form-control" name="startTime" placeholder="Start time (dd/mm/yy hh:mm)" style="margin-top: 5px;" required>

        <label for="inputEnd" class="sr-only">End Time</label>
        <input type="time" id="inputEnd" class="form-control" name="endTime" placeholder="End time (dd/mm/yy hh:mm)" style="margin-top: 5px;" required>

        <button class="btn btn-lg btn-success btn-block" type="submit" style="margin-top: 5px;">Create</button>
      </div>

    </form>
  </jsp:body>

</t:generic>