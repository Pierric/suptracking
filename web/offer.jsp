<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:generic>
  <jsp:attribute name="title">Offer</jsp:attribute>

  <jsp:body>
    <h2 class="form-signin-heading">Offer</h2>
    <p>
      <img src="librairies/car_offer.png" alt="Car Offer">
    </p>
    <p>Sécurisez vos voitures pour seulement 10$/mois sans engagement ! Inscrivez-vous et bénéficiez d'1 mois d'essai gratuit !</p>
  </jsp:body>

</t:generic>