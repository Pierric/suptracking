<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:generic>
  <jsp:attribute name="title">Admin Panel</jsp:attribute>

  <jsp:body>
    <h2 class="form-signin-heading">List of users</h2>

    <div class="col-md-8 col-md-offset-2" style="margin-top: 20px;">
      <table class="table table-striped">
        <thead>
        <tr>
          <th>Username</th>
          <th>First name</th>
          <th>Last name</th>
          <th>Latitude</th>
          <th>Longitude</th>
          <th>Last update</th>
          <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${users}" var="user">
          <tr>
            <td>${user.username}</td>
            <td>${user.firstName}</td>
            <td>${user.lastName}</td>
            <td>${user.latitude}</td>
            <td>${user.longitude}</td>
            <td>${user.lastUpdate}</td>
            <td><a href="${pageContext.request.contextPath}/admin/removeUser?id=${user.id}"><i class="fa fa-trash-o fa-fw fa-lg" style="color: red;"></i></a></td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>
  </jsp:body>

</t:generic>