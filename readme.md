# Créer la base de données

Un dump de la base de données est accessible via le fichier *"suptracking.sql"*. 

Autre solution pour créer la base de données,  il suffit d'utiliser le script situé dans le fichier *"script mysql.txt"*.  
Il est conseillé de créer les tables dans un schéma nommé "suptracking", ou le cas échéant de modifier dans le fichier persistence.xml la ligne suivante :
```
<property name="javax.persistence.jdbc.url" value="jdbc:mysql://localhost:3306/suptracking" />
```
avec les valeurs correspondant aux paramètres locaux.


#Utilisateurs

Deux utilisateurs sont déjà paramétrés, un administrateur et un utilisateur normal.

* admin:supinfo
* patricia:toutous

#Créé avec IntelliJ IDEA